Copyright (C) 2014  Ishara Upamal

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#define MAX_SPEED 255

#define BRAKE 0

#define PWM_A 5
#define PWM_B 6

#define MOTOR_RIGHT 4 //RIGHT
#define MOTOR_LEFT 7 //LEFT

#define DELAY_RIGHT_TURN 550
#define DELAY_LEFT_TURN 450

#define DELAY_HARD_BRAKE 50
#define DELAY_HARD_BRAKE2 20
#define DELAY_SOFT_BRAKE 1000
/**********************************************/

#define DELAY_NODE 500
int junction;
int whiteline;
int node_detect;
int box_detect;

int Jlock=0;
int node_lock;
int box_lock;

int tnode;
int tbox;

int twhite_time=0;
int tnode_time=0;

#define AVERAGE_NODE_TIME 100
#define AVERAGE_WHITE_TIME 50

/******************SENSORS********************/
int S[]={
  30,31,32,33,34,35,36,37};
int vS[8];

/*********************************************/

#define BUZZER 13
#define BRIGHT_LED 12

#define X 8
#define Y 10
int xd=0,yd=0;

int grid_map[X][Y]={
  {
    0,0,0,0,0,0,0,0,0,0                                      }
  ,{
    0,0,0,0,0,0,0,0,0,0                                      }
  ,{
    0,0,0,0,0,0,0,0,0,555                                      }
  ,{
    0,0,0,0,0,0,0,0,0,555                                      }
  ,{
    0,0,0,555,0,0,0,0,0,0                                      }
  ,{
    999,999,999,999,999,999,999,0,999,999                                      }
  ,{
    999,999,999,999,999,999,999,0,999,999                                      }
  ,{
    999,999,999,999,999,999,999,0,999,999                  }
};

/***************************************************************
****************************************************************
*******************Game_Play************************************/
#define GOAL0 222
#define GOAL1 333 
#define GOAL2 444
#define GOAL3 555
#define GOAL4 666
#define GOAL5 777

int goal_state=0;
int robot_next=0;
/*
if goal state 0;
  search co-ordinates of 222
  
if goal state 1;
  search co-ordinates of 333
  
  
  set goal according to co-ordinates
  
  robot move(){
    co-ordinate lock =0;
    
  
  }
  co-ordinate search(){
  if(co-ordinate_lock ==0 ){
    
  
  }
  }
*/
//map locations
int x=0;
int y=0;

// Wave Front Variables
int nothing=0;
int wall=999;
int goal=1;
int robot=254;

//starting robot/goal locations
int robot_x=0;
int robot_y=1;

int goal_x=1;
int goal_y=8;

int cell_size=330;//the size of the robot
int scan_one_cell=39;//lower number makes it see further



//temp variables
int counter=0;

//when searching for a node with a lower value
int minimum_node=250;
int min_node_location=0;
int reset_min=250;//anything above this number is a special item, ie a wall or robot

#define X  8;
#define Y 11;


//X is vertical, Y is horizontal
int temp_MAP[8][11]={
  {
    999,0,0,999,0,999,0,0,0,0,0  }
  ,{
    999,999,222,0,0,999,0,0,555,0,0  }
  ,{
    999,0,0,0,0,0,999,999,0,0,777  }
  ,{
    999,0,0,999,999,999,0,0,0,0,666  }
  ,{
    999,0,0,0,333,0,0,999,0,0,0  }
  ,{
    999,999,999,999,999,999,999,999,0,999,999  }
  ,
  {
    999,999,999,999,999,999,999,999,0,999,999  }
  ,{
    999,999,999,999,999,999,999,999,0,999,999  }
};
/******************************************************************
*******************Robot_Variables*********************************
*******************************************************************/
int move_robot;
void setup(){
  pinMode(PWM_A,OUTPUT);
  pinMode(PWM_B,OUTPUT);

  pinMode(BUZZER,OUTPUT);
  pinMode(BRIGHT_LED,OUTPUT);

  pinMode(MOTOR_RIGHT,OUTPUT);
  pinMode(MOTOR_LEFT,OUTPUT);

  for(int i = 0;i < 8;i++){
    pinMode(S[i],INPUT);

  }

  Serial.begin(9600);

}
void loop(){
  if(/*gameplay*/){
    //map locations
    
    
    
    
    y=0;
    goal();
  
  }
  if(dryrun){
    read_sensors(); 


  dry_run();
  
  }
 
}
void read_sensors(){
  for(int i=0;i<8;i++){
    vS[i]=!(digitalRead(S[i]));


  }
  initialize(); 


}


void dry_run(){
 
  if(whiteline){
    Jlock=0;
    forward_run();
   
    
    
  
  } else if((vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0)){
  reverse();
  }
  
  if(junction && !( xd == 4 && yd == 9 )){
    
      

    if(junction && Jlock == 0){
      buzzer();
      

     move_up();
      grid_map[xd][yd] == 0;
      
      grid_running();
      Jlock=1;

    }



    }/*
    else if(node_detect && node_lock == 0){
      node_lock=1;
      if(tnode != 0){
        tnode=millis();

        tnode_time=tbox -tnode;        

      }


    }
    
    
    if(box_detect){
      box_lock=1;
      tbox=millis();
      twhite_time=tnode - tbox;  
      tnode=0;

      if(AVERAGE_NODE_TIME < tnode_time && ! (AVERAGE_WHITE_TIME < twhite_time)){
        move_up();
        grid_map[xd][yd] == 888;
        grid_running();




      }
      else{
        move_up();
        grid_map[xd][yd] == 999;
       grid_running();




      }

    }




*/

}
  


void initialize(){
  junction = (vS[0]==1 && vS[1]==1 && vS[2]==1 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==1 && vS[7]==1);

  whiteline =( (vS[0]==1 && vS[1]==1 && vS[2]==1 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==1 && vS[2]==1 && vS[3]==1 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==1 && vS[3]==1 && vS[4]==1 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==1 && vS[5]==1 && vS[6]==1 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==1 && vS[6]==1 && vS[7]==1 ) ||
    (vS[0]==1 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) ||
    (vS[0]==1 && vS[1]==1 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) ||
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==1 && vS[7]==1 ) ||
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==1 ) ||
    (vS[0]==1 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ));

  node_detect=((vS[0]==1 && vS[1]==1 && vS[2] ==1 && vS[3] ==0 && vS[4] ==0 && vS[5] ==1 && vS[6] ==1 && vS[7] ==1 ) ||
    (vS[0]==1 && vS[1]==1 && vS[2]==0 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==1 && vS[7]==1 ) ||
    (vS[0]==1 && vS[1]==1 && vS[2]==1 && vS[3]==1 && vS[4]==0 && vS[5]==1 && vS[6]==1 && vS[7]==1 ));

  box_detect=((vS[0]==0 && vS[1]==0 && vS[2]==1 && vS[3]==1 && vS[4]==1 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) ||
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==0 && vS[7]==1 ));


}

void move_up(){
  if(xd % 2 == 0){
   
     yd++;
   
  }
  else if(xd % 2 == 1 ){
   
    yd--;
    
  }

}

void grid_running(){

  if(xd % 2 == 0){
    if(yd == 9){
      
     
      hard_brake();
      low_speed();
      delay(250);
      hard_brake();
      
      turn_right();
      
      hard_brake();
          
      
       xd++;
       yd++;
       
       

      

    }
    else if(yd == 1 && xd != 0){
      hard_brake();
      low_speed();
      delay(250);
      hard_brake();

      turn_left();
       
      
      hard_brake();
      

    }
    


  }
  else if(xd % 2 == 1){
    if(yd == 9){
      hard_brake2();
      
      low_speed();
      delay(250);
      hard_brake2();
      
      turn_right();
      hard_brake2();
      

    }
    else if(y == 1){
      
      hard_brake2();
      
      low_speed();
      delay(250);
      hard_brake2();
      
      turn_left();
      
      hard_brake2();
      
      xd++;
      yd--;
      
      
      

      

    }
    

  }

}
void soft_brake(){
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);
  delay(DELAY_SOFT_BRAKE);
}
void hard_brake(){
  if(digitalRead(MOTOR_RIGHT) == HIGH && (digitalRead(MOTOR_LEFT) == HIGH)){
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
    delay(DELAY_HARD_BRAKE);

  }
  else if(digitalRead(MOTOR_RIGHT) == LOW &&(digitalRead(MOTOR_LEFT) == LOW)){
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
    delay(DELAY_HARD_BRAKE);

  }
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);

}
void hard_brake2(){
  if(digitalRead(MOTOR_RIGHT) == HIGH && (digitalRead(MOTOR_LEFT) == HIGH)){
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
    delay(DELAY_HARD_BRAKE2);

  }
  else if(digitalRead(MOTOR_RIGHT) == LOW &&(digitalRead(MOTOR_LEFT) == LOW)){
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
    delay(DELAY_HARD_BRAKE2);

  }
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);

}
void turn_left(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,HIGH);
  digitalWrite(MOTOR_LEFT,LOW);
  delay(DELAY_LEFT_TURN);
}
void turn_right(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,LOW);
  digitalWrite(MOTOR_LEFT,HIGH);
  delay(DELAY_RIGHT_TURN);
}
void buzzer(){
  digitalWrite(BUZZER,HIGH);
  delay(30);
  digitalWrite(BUZZER,LOW);
  delay(30);
}
void forward_run(){
  if(vS[2] ==1 && vS[3] ==1 && vS[4]==1){
      
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,220);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
    

    }else if(vS[3] == 1 && vS[4] ==1 && vS[5] ==1){
      
      analogWrite(PWM_A,220);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      

    }else if(vS[1] == 1 && vS[2]==1 && vS[3]==1 ){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,200);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      
      


    }
     else if(vS[6] == 1 && vS[5]==1 && vS[4]==1 ){
      
      analogWrite(PWM_A,200);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      
      


    }else if(vS[0] == 1 && vS[1]==1){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,180);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
         
      
    }else if(vS[7] == 1 && vS[6]==1){
      
      analogWrite(PWM_A,180);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      }
     
    else if(vS[0] == 1){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,BRAKE);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      }
      

      else if(vS[7] == 1){
      
      analogWrite(PWM_A,BRAKE);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      

    }else{
      analogWrite(PWM_A,BRAKE);
      analogWrite(PWM_B,BRAKE);
      digitalWrite(MOTOR_RIGHT,LOW);
      digitalWrite(MOTOR_LEFT,LOW);
    
    }
    



}
void low_speed(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,HIGH);
  digitalWrite(MOTOR_LEFT,HIGH);

}
void reverse(){
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);
  digitalWrite(MOTOR_RIGHT,LOW);
  digitalWrite(MOTOR_LEFT,LOW);
  delay(3);
 
  
  

}
/***************************************************************
*****************EEPROM*****************************************
*****************************************************************/

void write_to_EEPROM(){
  grid_size=sizeof(grid_map)/sizeof(grid_map[0][0]);
  for(int i=0;i<grid_size;i++){
    for(int j=0;j<X;j++){
      for(int k=0;k<Y;k++,i++){
        EEPROM.write(i,grid_map[j][k]);
      }
    }
  }
}

void form_map(){
  MAP[X][Y];
  for(int i = 0;i < grid_size;i++){
    for(int j = 0;j < X;j++){
      for(int k = 0;k < Y;k++,i++){
        MAP[j][k]=EEPROM.read(i);
      }
    }
  }


}





/****************************************************************
*****************************************************************
*************Game_Play_methods***********************************/
void game_run(){
 while(whiteline){
   if(vS[2] ==1 && vS[3] ==1 && vS[4]==1){
      
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,220);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
    

    }else if(vS[3] == 1 && vS[4] ==1 && vS[5] ==1){
      
      analogWrite(PWM_A,220);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      

    }else if(vS[1] == 1 && vS[2]==1 && vS[3]==1 ){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,200);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      
      


    }
     else if(vS[6] == 1 && vS[5]==1 && vS[4]==1 ){
      
      analogWrite(PWM_A,200);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      
      


    }else if(vS[0] == 1 && vS[1]==1){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,180);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
         
      
    }else if(vS[7] == 1 && vS[6]==1){
      
      analogWrite(PWM_A,180);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      }
     
    else if(vS[0] == 1){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,BRAKE);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      }
      

      else if(vS[7] == 1){
      
      analogWrite(PWM_A,BRAKE);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      

    }else{
      analogWrite(PWM_A,BRAKE);
      analogWrite(PWM_B,BRAKE);
      digitalWrite(MOTOR_RIGHT,LOW);
      digitalWrite(MOTOR_LEFT,LOW);
    
    }
    
 
 }
}

void find_goal(){
  if(goalstate == 0){
    getcoordinates(222);
    robot_next=propagate_wavefrontrobot_x,robot_y,goal_x,goal_y);
  
  }else if(goalstate == 1){
    getcoordinates(333);
    robot_next=propagate_wavefrontrobot_x,robot_y,goal_x,goal_y);
  }
  else if(goalstate == 2){
    getcoordinates(444);
    robot_next=propagate_wavefrontrobot_x,robot_y,goal_x,goal_y);
  }
  else if(goalstate == 3){
    getcoordinates(555);
    robot_next=propagate_wavefrontrobot_x,robot_y,goal_x,goal_y);
  }
  else if(goalstate == 4){
    getcoordinates(666);
    robot_next=propagate_wavefrontrobot_x,robot_y,goal_x,goal_y);
  }
  else if(goalstate == 5){
    getcoordinates(777);
    robot_next=propagate_wavefrontrobot_x,robot_y,goal_x,goal_y);
  }
  

}

int propagate_wavefront(int robot_x, int robot_y, int goal_x, int goal_y)
{
  //clear old wavefront
  unpropagate(robot_x, robot_y);

  counter=0;//reset the counter for each run!
  while(counter<50)//allows for recycling until robot is found
  {
    x=0;
    y=0;
    while(x<X && y<Y)//while the map hasnt been fully scanned
    {
      //if this location is a wall or the goal, just ignore it
      if (temp_MAP[x][y] != wall && temp_MAP[x][y] != goal)
      {	
        //a full trail to the robot has been located, finished!
        if (min_surrounding_node_value(x, y) < reset_min && temp_MAP[x][y]==robot)
        {
          //finshed! tell robot to start moving down path
          return min_node_location;
        }
        //record a value in to this node
        else if (minimum_node!=reset_min)//if this isnt here, 'nothing' will go in the location
            temp_MAP[x][y]=minimum_node+1;
      }

      //go to next node and/or row
      y++;
      if (y==Y && x!=Y)
      {
        x++;
        y=0;
      }
    }
    counter++;
  }
  return 0;
}

void unpropagate(int robot_x, int robot_y)//clears old path to determine new path
{
  //stay within boundary
  for(x=0; x<6; x++)
    for(y=0; y<6; y++)
      if (temp_MAP[x][y] != wall && temp_MAP[x][y] != goal) //if this location is something, just ignore it
        temp_MAP[x][y] = nothing;//clear that space

  //store robot location in map
  temp_MAP[robot_x][robot_y]=robot;
  //store robot location in map
  temp_MAP[goal_x][goal_y]=goal;
}

//if no solution is found, delete all walls from map
void clear_map(void)
{	
  for(x=0;x<6;x++)
    for(y=0;y<6;y++)
      if (temp_MAP[x][y] != robot && temp_MAP[x][y] != goal)
        temp_MAP[x][y]=nothing;
}

//this function looks at a node and returns the lowest value around that node
//1 is up, 2 is right, 3 is down, and 4 is left (clockwise)
int min_surrounding_node_value(int x, int y)
{
  minimum_node=reset_min;//reset minimum

  //down
  if(x < X)//not out of boundary
    if  (temp_MAP[x+1][y] < minimum_node && temp_MAP[x+1][y] != nothing)//find the lowest number node, and exclude empty nodes (0's)
    {
      minimum_node = temp_MAP[x+1][y];
      min_node_location=3;
    }

  //up
  if(x > 0)
    if  (temp_MAP[x-1][y] < minimum_node && temp_MAP[x-1][y] != nothing)
    {
      minimum_node = temp_MAP[x-1][y];
      min_node_location=1;
    }

  //right
  if(y < Y)
    if  (temp_MAP[x][y+1] < minimum_node && temp_MAP[x][y+1] != nothing)
    {
      minimum_node = temp_MAP[x][y+1];
      min_node_location=2;
    }

  //left
  if(y > 0)
    if  (temp_MAP[x][y-1] < minimum_node && temp_MAP[x][y-1] != nothing)
    {
      minimum_node = temp_MAP[x][y-1];
      min_node_location=4;
    }

  return minimum_node;
}

void getcoordinates(int value){
  int i;
  int j;
  for(i=0;i < X ;i++){
  
  }for(j=1;j<Y;j++){
    if(temp_MAP[i][j] == value){
      goal_x=i;
      goal_y=j;
    
    }
  
  }
  

}
/**************************************************************************************************************/
void test(){
  if(!(robot_x == goal_x && robot_y == goal_y) ){
    print_temp_map();
    move_robot=propagate_wavefront(robot_x,robot_y,goal_x,goal_y);
    Serial.println(move_robot);
    
    if(move_robot == 2){
      robot_y += 1;
      temp_MAP[x][robot_y - 1] = 0; 
      temp_MAP[robot_x][robot_y]=robot;


    }
    if(move_robot == 3){
      robot_x += 1;
      temp_MAP[robot_x - 1][y] = 0; 
      temp_MAP[robot_x][robot_y]=robot;
    }
    if (move_robot ==4){
      robot_y -= 1;
      temp_MAP[x][robot_y + 1] = 0;
     temp_MAP[robot_x][robot_y]=robot; 
    }
    if (move_robot ==1){
      robot_x -= 1;
      temp_MAP[robot_x + 1][y] = 0;
      temp_MAP[robot_x][robot_y]=robot;
      
    }
    

}
}
void print_temp_map(){
  Serial.print("Temp map :");
  Serial.println(" ");
  for(int i=0;i<X;i++){
    Serial.print("| ");


    for(int j=0;j<Y;j++){
      Serial.print("[");
      Serial.print(temp_MAP[i][j]);
      Serial.print("]");
      Serial.print(" ");


    }
    Serial.println(" |");
  }


}

