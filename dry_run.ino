Copyright (C) 2014  Ishara Upamal

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


#define MAX_SPEED 255

#define BRAKE 0

#define PWM_A 5
#define PWM_B 6

#define MOTOR_RIGHT 4 //RIGHT
#define MOTOR_LEFT 7 //LEFT

#define DELAY_RIGHT_TURN 450
#define DELAY_LEFT_TURN 400

#define DELAY_HARD_BRAKE 50
#define DELAY_HARD_BRAKE2 20
#define DELAY_SOFT_BRAKE 1000
/**********************************************/

#define DELAY_NODE 500
int junction;
int whiteline;
int node_detect;
int box_detect;

int Jlock=0;
int node_lock;
int box_lock;

int tnode;
int tbox;

int twhite_time=0;
int tnode_time=0;

#define AVERAGE_NODE_TIME 100
#define AVERAGE_WHITE_TIME 50
/*
detect node:
 time in node=tbox-tnode
 
 detect white:
 time in white=tnode-tbox;
 
 if(time in node > avarage node time && !(time in white > avarage time in white) ){
 set point as a box deposit
 else{
 set point as a node
 }
 
 }
 */
/******************SENSORS********************/
int S[]={
  30,31,32,33,34,35,36,37};
int vS[8];

/*********************************************/

#define BUZZER 13
#define BRIGHT_LED 12

#define X 8
#define Y 10
int x=0,y=0;

int grid_map[X][Y]={
  {
    0,0,0,0,0,0,0,0,0,0                                      }
  ,{
    0,0,0,0,0,0,0,0,0,0                                      }
  ,{
    0,0,0,0,0,0,0,0,0,555                                      }
  ,{
    0,0,0,0,0,0,0,0,0,555                                      }
  ,{
    0,0,0,555,0,0,0,0,0,0                                      }
  ,{
    999,999,999,999,999,999,999,0,999,999                                      }
  ,{
    999,999,999,999,999,999,999,0,999,999                                      }
  ,{
    999,999,999,999,999,999,999,0,999,999                  }
};


/************************************************************
 *******************Game Play*********************************
 *************************************************************/
int MAP[X][Y];
int mapX=8,mapY=10;
int robotDir;
int robotX;
int robotY;

/************************************************************/

void setup(){
  pinMode(PWM_A,OUTPUT);
  pinMode(PWM_B,OUTPUT);

  pinMode(BUZZER,OUTPUT);
  pinMode(BRIGHT_LED,OUTPUT);

  pinMode(MOTOR_RIGHT,OUTPUT);
  pinMode(MOTOR_LEFT,OUTPUT);

  for(int i = 0;i < 8;i++){
    pinMode(S[i],INPUT);

  }

  Serial.begin(9600);

}
void loop(){
  
  read_sensors(); 


  dry_run();
}

void read_sensors(){
  for(int i=0;i<8;i++){
    vS[i]=!(digitalRead(S[i]));


  }
  initialize(); 


}


void dry_run(){
 
  if(whiteline){
    Jlock=0;
    forward_run();
   
    
    
  
  } else if((vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0)){
  reverse();
  }
  
  if(junction && !( x == 4 && y == 9 )){
    
      

    if(junction && Jlock == 0){
      buzzer();
      

     move_up();
      grid_map[x][y] == 0;
      
      grid_running();
      Jlock=1;

    }



    }
    
    /*
    else if(node_detect && node_lock == 0){
      node_lock=1;
      if(tnode != 0){
        tnode=millis();

        tnode_time=tbox -tnode;        

      }


    }
    
    
    if(box_detect){
      box_lock=1;
      tbox=millis();
      twhite_time=tnode - tbox;  
      tnode=0;

      if(AVERAGE_NODE_TIME < tnode_time && ! (AVERAGE_WHITE_TIME < twhite_time)){
        move_up();
        grid_map[x][y] == 888;
        grid_running();




      }
      else{
        move_up();
        grid_map[x][y] == 999;
       grid_running();




      }

    }




*/

}
  


void initialize(){
  junction = (vS[0]==1 && vS[1]==1 && vS[2]==1 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==1 && vS[7]==1);

  whiteline =( (vS[0]==1 && vS[1]==1 && vS[2]==1 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==1 && vS[2]==1 && vS[3]==1 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==1 && vS[3]==1 && vS[4]==1 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==0 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==1 && vS[5]==1 && vS[6]==1 && vS[7]==0 ) || 
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==1 && vS[6]==1 && vS[7]==1 ) ||
    (vS[0]==1 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) ||
    (vS[0]==1 && vS[1]==1 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) ||
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==1 && vS[7]==1 ) ||
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==1 ) ||
    (vS[0]==1 && vS[1]==0 && vS[2]==0 && vS[3]==0 && vS[4]==0 && vS[5]==0 && vS[6]==0 && vS[7]==0 ));

  node_detect=((vS[0]==1 && vS[1]==1 && vS[2] ==1 && vS[3] ==0 && vS[4] ==0 && vS[5] ==1 && vS[6] ==1 && vS[7] ==1 ) ||
    (vS[0]==1 && vS[1]==1 && vS[2]==0 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==1 && vS[7]==1 ) ||
    (vS[0]==1 && vS[1]==1 && vS[2]==1 && vS[3]==1 && vS[4]==0 && vS[5]==1 && vS[6]==1 && vS[7]==1 ));

  box_detect=((vS[0]==0 && vS[1]==0 && vS[2]==1 && vS[3]==1 && vS[4]==1 && vS[5]==0 && vS[6]==0 && vS[7]==0 ) ||
    (vS[0]==0 && vS[1]==0 && vS[2]==0 && vS[3]==1 && vS[4]==1 && vS[5]==1 && vS[6]==0 && vS[7]==1 ));


}

void move_up(){
  if(x % 2 == 0){
   
     y++;
   
   


  }
  else if(x % 2 == 1 ){
   
    y--;
    

  }

}

void grid_running(){

  if(x % 2 == 0){
    if(y == 9){
      
     
      hard_brake();
      low_speed();
      delay(250);
      hard_brake2();
      
      turn_right();
      
      hard_brake2();
          
      
       x++;
       y++;
       
       

      

    }
    else if(y == 1 && x != 0){
      hard_brake();
      low_speed();
      delay(250);
      hard_brake();

      turn_left();
       
      
      hard_brake();
      

    }
    


  }
  else if(x % 2 == 1){
    if(y == 9){
      hard_brake2();
      
      low_speed();
      delay(250);
      hard_brake2();
      
      turn_right();
      hard_brake2();
      

    }
    else if(y == 1){
      
      hard_brake();
      
      low_speed();
      delay(250);
      hard_brake2();
      
      turn_left();
      
      hard_brake2();
      
      x++;
      y--;
      
      
      

      

    }
    

  }

}
void soft_brake(){
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);
  delay(DELAY_SOFT_BRAKE);
}
void hard_brake(){
  if(digitalRead(MOTOR_RIGHT) == HIGH && (digitalRead(MOTOR_LEFT) == HIGH)){
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
    delay(DELAY_HARD_BRAKE);

  }
  else if(digitalRead(MOTOR_RIGHT) == LOW &&(digitalRead(MOTOR_LEFT) == LOW)){
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
    delay(DELAY_HARD_BRAKE);

  }
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);

}
void hard_brake2(){
  if(digitalRead(MOTOR_RIGHT) == HIGH && (digitalRead(MOTOR_LEFT) == HIGH)){
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
    delay(DELAY_HARD_BRAKE2);

  }
  else if(digitalRead(MOTOR_RIGHT) == LOW &&(digitalRead(MOTOR_LEFT) == LOW)){
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
    delay(DELAY_HARD_BRAKE2);

  }
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);

}
void turn_left(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,HIGH);
  digitalWrite(MOTOR_LEFT,LOW);
  delay(DELAY_LEFT_TURN);
}
void turn_right(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,LOW);
  digitalWrite(MOTOR_LEFT,HIGH);
  delay(DELAY_RIGHT_TURN);
}
void buzzer(){
  digitalWrite(BUZZER,HIGH);
  delay(30);
  digitalWrite(BUZZER,LOW);
  delay(30);
}
void forward_run(){
  if(vS[2] ==1 && vS[3] ==1 && vS[4]==1){
      
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,220);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
    

    }else if(vS[3] == 1 && vS[4] ==1 && vS[5] ==1){
      
      analogWrite(PWM_A,220);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      

    }else if(vS[1] == 1 && vS[2]==1 && vS[3]==1 ){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,200);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      
      


    }
     else if(vS[6] == 1 && vS[5]==1 && vS[4]==1 ){
      
      analogWrite(PWM_A,200);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      
      


    }else if(vS[0] == 1 && vS[1]==1){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,180);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
         
      
    }else if(vS[7] == 1 && vS[6]==1){
      
      analogWrite(PWM_A,180);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      }
     
    else if(vS[0] == 1){
      
      analogWrite(PWM_A,MAX_SPEED);
      analogWrite(PWM_B,BRAKE);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      }
      

      else if(vS[7] == 1){
      
      analogWrite(PWM_A,BRAKE);
      analogWrite(PWM_B,MAX_SPEED);
      digitalWrite(MOTOR_RIGHT,HIGH);
      digitalWrite(MOTOR_LEFT,HIGH);
      
      

    }else{
      analogWrite(PWM_A,BRAKE);
      analogWrite(PWM_B,BRAKE);
      digitalWrite(MOTOR_RIGHT,LOW);
      digitalWrite(MOTOR_LEFT,LOW);
    
    }
    



}
void low_speed(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,HIGH);
  digitalWrite(MOTOR_LEFT,HIGH);

}
void reverse(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,LOW);
  digitalWrite(MOTOR_LEFT,LOW);
  delay(3);
 
  
  

}
