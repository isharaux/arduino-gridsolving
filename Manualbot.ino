Copyright (C) 2014  Ishara Upamal

This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


#include <PS2X_lib.h>

PS2X ps2x;

int error = 0; 
byte type = 0;
byte vibrate = 0;

#define SPEED1 255
#define SPEED2 255
#define SPEED3 255
#define SPEED4 255

#define SPEED5 180
#define SPEED6 180
#define SPEED7 180
#define SPEED8 180

#define SPEED9 100
#define SPEED10 100
#define SPEED11 100
#define SPEED12 100

#define PWM_A 9
#define PWM_B 12
#define PWM_C 11
#define PWM_D 10
#define MOTOR_A_DIR1 0
#define MOTOR_A_DIR2 1
#define MOTOR_B_DIR1 2
#define MOTOR_B_DIR2 3
#define MOTOR_C_DIR1 4
#define MOTOR_C_DIR2 5
#define MOTOR_D_DIR1 6
#define MOTOR_D_DIR2 7


#define DATA_PIN 14
#define COMMAND_PIN 15
#define ATTENTION_PIN 16
#define CLOCK_PIN 17


unsigned int data = 0;
byte OK=false;
int speed_type=0;



void setup(){
  pinMode(PWM_A,OUTPUT);
  pinMode(PWM_B,OUTPUT);
  pinMode(PWM_C,OUTPUT);
  pinMode(PWM_D,OUTPUT);


  pinMode(MOTOR_A_DIR1,OUTPUT);
  pinMode(MOTOR_A_DIR2,OUTPUT);
  pinMode(MOTOR_B_DIR1,OUTPUT);
  pinMode(MOTOR_B_DIR2,OUTPUT);
  pinMode(MOTOR_C_DIR1,OUTPUT);
  pinMode(MOTOR_C_DIR2,OUTPUT);
  pinMode(MOTOR_D_DIR1,OUTPUT);
  pinMode(MOTOR_D_DIR2,OUTPUT);

  //Setup Ps2 controller
  error=ps2x.config_gamepad(CLOCK_PIN,COMMAND_PIN,ATTENTION_PIN,DATA_PIN,true,true);
  byte controllerType = ps2x.readType();
  if (controllerType == 1 && error ==0){
    OK = true;


  }
}

void loop(){
  if(OK){
    button_functions();

  }





}
void button_functions(){
  ps2x.read_gamepad(false, vibrate);          

  if(ps2x.Button(PSB_START)){

  }                   

  if(ps2x.Button(PSB_SELECT)){
  }



  if(ps2x.Button(PSB_PAD_UP)) {         

    Serial.println(ps2x.Analog(PSAB_PAD_UP), DEC);
  }
  if(ps2x.Button(PSB_PAD_RIGHT)){

    Serial.println(ps2x.Analog(PSAB_PAD_RIGHT), DEC);
  }
  if(ps2x.Button(PSB_PAD_LEFT)){

    Serial.println(ps2x.Analog(PSAB_PAD_LEFT), DEC);
  }
  if(ps2x.Button(PSB_PAD_DOWN)){

    Serial.println(ps2x.Analog(PSAB_PAD_DOWN), DEC);
  }   


  vibrate = ps2x.Analog(PSAB_BLUE);        

  if (ps2x.NewButtonState())               



    if(ps2x.Button(PSB_L3)){
    }

  if(ps2x.Button(PSB_R3)){
  }

  if(ps2x.Button(PSB_L2)){
  }

  if(ps2x.Button(PSB_R2)){
  }


  if(ps2x.Button(PSB_GREEN)){


  }






  if(ps2x.ButtonPressed(PSB_RED)) {


  }            


  if(ps2x.ButtonReleased(PSB_PINK)){


  }                 

  if(ps2x.NewButtonState(PSB_BLUE)) {


  }           



  if((ps2x.Analog(PSS_RY)) < 128){
    turn_left();
  }
  if(ps2x.Analog(PSS_RY) > 128){
    turn_right();

  }
  if((ps2x.Analog(PSS_LX)) > 128){
    backward();

  }
  if((ps2x.Analog(PSS_LX)) < 128){
    forward();

  }
  if((ps2x.Analog(PSS_LX) == 128) && (ps2x.Analog(PSS_LY) == 128) && (ps2x.Analog(PSS_RX) == 128) && (ps2x.Analog(PSS_RY) == 128) ){
    stop_brake();
  }






  delay(50);

}


/**************************************************************
 ***************************************************************
 **************MOTOR FUNCTIONS**********************************
 ***************************************************************/

void forward(){
  if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)){
    speed_type = 1;

  }
  else if(ps2x.Button(PSB_L2) || ps2x.Button(PSB_R2)){
    speed_type = 2;

  }
  else{
    speed_type = 0;
  }
  if(speed_type == 1){

    analogWrite(PWM_A,SPEED1);
    analogWrite(PWM_B,SPEED2);
    analogWrite(PWM_C,SPEED3);
    analogWrite(PWM_D,SPEED4);

    digitalWrite(MOTOR_A_DIR1,HIGH);
    digitalWrite(MOTOR_A_DIR2,LOW);
    digitalWrite(MOTOR_B_DIR1,HIGH);
    digitalWrite(MOTOR_B_DIR2,LOW);
    digitalWrite(MOTOR_C_DIR1,HIGH);
    digitalWrite(MOTOR_C_DIR2,LOW);
    digitalWrite(MOTOR_D_DIR1,HIGH);
    digitalWrite(MOTOR_D_DIR2,LOW);

  }
  else if(speed_type == 2){

    analogWrite(PWM_A,SPEED9);
    analogWrite(PWM_B,SPEED10);
    analogWrite(PWM_C,SPEED11);
    analogWrite(PWM_D,SPEED12);

    digitalWrite(MOTOR_A_DIR1,HIGH);
    digitalWrite(MOTOR_A_DIR2,LOW);
    digitalWrite(MOTOR_B_DIR1,HIGH);
    digitalWrite(MOTOR_B_DIR2,LOW);
    digitalWrite(MOTOR_C_DIR1,HIGH);
    digitalWrite(MOTOR_C_DIR2,LOW);
    digitalWrite(MOTOR_D_DIR1,HIGH);
    digitalWrite(MOTOR_D_DIR2,LOW);

  }
  else if(speed_type == 0){

    analogWrite(PWM_A,SPEED5);
    analogWrite(PWM_B,SPEED6);
    analogWrite(PWM_C,SPEED7);
    analogWrite(PWM_D,SPEED8);

    digitalWrite(MOTOR_A_DIR1,HIGH);
    digitalWrite(MOTOR_A_DIR2,LOW);
    digitalWrite(MOTOR_B_DIR1,HIGH);
    digitalWrite(MOTOR_B_DIR2,LOW);
    digitalWrite(MOTOR_C_DIR1,HIGH);
    digitalWrite(MOTOR_C_DIR2,LOW);
    digitalWrite(MOTOR_D_DIR1,HIGH);
    digitalWrite(MOTOR_D_DIR2,LOW);

  }




}




void turn_right(){
  if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)){
    speed_type = 1;

  }
  else if(ps2x.Button(PSB_L2) || ps2x.Button(PSB_R2)){
    speed_type = 2;

  }
  else{
    speed_type = 0;
  }
  if(speed_type == 0){
    analogWrite(PWM_A,SPEED5);
    analogWrite(PWM_B,SPEED6);
    analogWrite(PWM_C,SPEED7);
    analogWrite(PWM_D,SPEED8);

    digitalWrite(MOTOR_A_DIR1,LOW);
    digitalWrite(MOTOR_A_DIR2,HIGH);
    digitalWrite(MOTOR_B_DIR1,LOW);
    digitalWrite(MOTOR_B_DIR2,HIGH);
    digitalWrite(MOTOR_C_DIR1,HIGH);
    digitalWrite(MOTOR_C_DIR2,LOW);
    digitalWrite(MOTOR_D_DIR1,HIGH);
    digitalWrite(MOTOR_D_DIR2,LOW);
  }
  if(speed_type == 1){
    analogWrite(PWM_A,SPEED1);
    analogWrite(PWM_B,SPEED2);
    analogWrite(PWM_C,SPEED3);
    analogWrite(PWM_D,SPEED4);

    digitalWrite(MOTOR_A_DIR1,LOW);
    digitalWrite(MOTOR_A_DIR2,HIGH);
    digitalWrite(MOTOR_B_DIR1,LOW);
    digitalWrite(MOTOR_B_DIR2,HIGH);
    digitalWrite(MOTOR_C_DIR1,HIGH);
    digitalWrite(MOTOR_C_DIR2,LOW);
    digitalWrite(MOTOR_D_DIR1,HIGH);
    digitalWrite(MOTOR_D_DIR2,LOW);
  }
  if(speed_type == 2){
    analogWrite(PWM_A,SPEED9);
    analogWrite(PWM_B,SPEED10);
    analogWrite(PWM_C,SPEED11);
    analogWrite(PWM_D,SPEED12);

    digitalWrite(MOTOR_A_DIR1,LOW);
    digitalWrite(MOTOR_A_DIR2,HIGH);
    digitalWrite(MOTOR_B_DIR1,LOW);
    digitalWrite(MOTOR_B_DIR2,HIGH);
    digitalWrite(MOTOR_C_DIR1,HIGH);
    digitalWrite(MOTOR_C_DIR2,LOW);
    digitalWrite(MOTOR_D_DIR1,HIGH);
    digitalWrite(MOTOR_D_DIR2,LOW);
  }




}
void turn_left(){
  if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)){
    speed_type = 1;

  }
  else if(ps2x.Button(PSB_L2) || ps2x.Button(PSB_R2)){
    speed_type = 2;

  }
  else{
    speed_type = 0;
  }
  if(speed_type=0){
    analogWrite(PWM_A,SPEED5);
    analogWrite(PWM_B,SPEED6);
    analogWrite(PWM_C,SPEED7);
    analogWrite(PWM_D,SPEED8);

    digitalWrite(MOTOR_A_DIR1,HIGH);
    digitalWrite(MOTOR_A_DIR2,LOW);
    digitalWrite(MOTOR_B_DIR1,HIGH);
    digitalWrite(MOTOR_B_DIR2,LOW);
    digitalWrite(MOTOR_C_DIR1,LOW);
    digitalWrite(MOTOR_C_DIR2,HIGH);
    digitalWrite(MOTOR_D_DIR1,LOW);
    digitalWrite(MOTOR_D_DIR2,HIGH);
  }
  else if(speed_type=1){
    analogWrite(PWM_A,SPEED1);
    analogWrite(PWM_B,SPEED2);
    analogWrite(PWM_C,SPEED3);
    analogWrite(PWM_D,SPEED4);

    digitalWrite(MOTOR_A_DIR1,HIGH);
    digitalWrite(MOTOR_A_DIR2,LOW);
    digitalWrite(MOTOR_B_DIR1,HIGH);
    digitalWrite(MOTOR_B_DIR2,LOW);
    digitalWrite(MOTOR_C_DIR1,LOW);
    digitalWrite(MOTOR_C_DIR2,HIGH);
    digitalWrite(MOTOR_D_DIR1,LOW);
    digitalWrite(MOTOR_D_DIR2,HIGH);
  }
  else 
    if(speed_type=2){
    analogWrite(PWM_A,SPEED9);
    analogWrite(PWM_B,SPEED10);
    analogWrite(PWM_C,SPEED11);
    analogWrite(PWM_D,SPEED12);

    digitalWrite(MOTOR_A_DIR1,HIGH);
    digitalWrite(MOTOR_A_DIR2,LOW);
    digitalWrite(MOTOR_B_DIR1,HIGH);
    digitalWrite(MOTOR_B_DIR2,LOW);
    digitalWrite(MOTOR_C_DIR1,LOW);
    digitalWrite(MOTOR_C_DIR2,HIGH);
    digitalWrite(MOTOR_D_DIR1,LOW);
    digitalWrite(MOTOR_D_DIR2,HIGH);
  }


}
void stop_brake(){
  digitalWrite(MOTOR_A_DIR1,LOW);
  digitalWrite(MOTOR_A_DIR2,LOW);
  digitalWrite(MOTOR_B_DIR1,LOW);
  digitalWrite(MOTOR_B_DIR2,LOW);
  digitalWrite(MOTOR_C_DIR1,LOW);
  digitalWrite(MOTOR_C_DIR2,LOW);
  digitalWrite(MOTOR_D_DIR1,LOW);
  digitalWrite(MOTOR_D_DIR2,LOW);

}
void backward(){
  if(ps2x.Button(PSB_L1) || ps2x.Button(PSB_R1)){
    speed_type = 1;

  }
  else if(ps2x.Button(PSB_L2) || ps2x.Button(PSB_R2)){
    speed_type = 2;

  }
  else{
    speed_type = 0;
  }
  if(speed_type = 0){
    analogWrite(PWM_A,SPEED5);
    analogWrite(PWM_B,SPEED6);
    analogWrite(PWM_C,SPEED7);
    analogWrite(PWM_D,SPEED8);

    digitalWrite(MOTOR_A_DIR1,LOW);
    digitalWrite(MOTOR_A_DIR2,HIGH);
    digitalWrite(MOTOR_B_DIR1,LOW);
    digitalWrite(MOTOR_B_DIR2,HIGH);
    digitalWrite(MOTOR_C_DIR1,LOW);
    digitalWrite(MOTOR_C_DIR2,HIGH);
    digitalWrite(MOTOR_D_DIR1,LOW);
    digitalWrite(MOTOR_D_DIR2,HIGH);
  }
  else if(speed_type = 1){
    analogWrite(PWM_A,SPEED1);
    analogWrite(PWM_B,SPEED2);
    analogWrite(PWM_C,SPEED3);
    analogWrite(PWM_D,SPEED4);

    digitalWrite(MOTOR_A_DIR1,LOW);
    digitalWrite(MOTOR_A_DIR2,HIGH);
    digitalWrite(MOTOR_B_DIR1,LOW);
    digitalWrite(MOTOR_B_DIR2,HIGH);
    digitalWrite(MOTOR_C_DIR1,LOW);
    digitalWrite(MOTOR_C_DIR2,HIGH);
    digitalWrite(MOTOR_D_DIR1,LOW);
    digitalWrite(MOTOR_D_DIR2,HIGH);
  }
  else if(speed_type = 2){
    analogWrite(PWM_A,SPEED9);
    analogWrite(PWM_B,SPEED10);
    analogWrite(PWM_C,SPEED11);
    analogWrite(PWM_D,SPEED12);

    digitalWrite(MOTOR_A_DIR1,LOW);
    digitalWrite(MOTOR_A_DIR2,HIGH);
    digitalWrite(MOTOR_B_DIR1,LOW);
    digitalWrite(MOTOR_B_DIR2,HIGH);
    digitalWrite(MOTOR_C_DIR1,LOW);
    digitalWrite(MOTOR_C_DIR2,HIGH);
    digitalWrite(MOTOR_D_DIR1,LOW);
    digitalWrite(MOTOR_D_DIR2,HIGH);
  }


}





