

/*
Copyright (C) 2014  Ishara Upamal
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

/******************************************************************
 *******************************************************************
 *******************************************************************/
#include <EEPROM.h>
#include <Servo.h>

#define MAX_SPEED 255
#define SPEED1 220
#define SPEED2 200 
#define SPEED3 180

#define BRAKE 0

#define PWM_A 5 //Right Motor
#define PWM_B 6 //Left Motor

#define MOTOR_RIGHT 4 //RIGHT
#define MOTOR_LEFT 7 //LEFT

#define DELAY_RIGHT_TURN 550
#define DELAY_LEFT_TURN 450

#define DELAY_HARD_BRAKE 50
#define DELAY_HARD_BRAKE2 20
#define DELAY_SOFT_BRAKE 1000

/******************************************************************
 *******************************************************************
 *******************************************************************/
int Whiteline;
int Junction;
int Dry_end;

int Node;
int Box_deposit;

int T_black;
int T_white;

int Junctionlock = 0;

#define AVERAGE_NODE_TIME 100
#define AVERAGE_WHITE_TIME 50

/******************************************************************
 ******************************************************************
 ******************************************************************/
int S[]={
  30,31,32,3,34,35,36,37};
int vS[8];

/******************************************************************
 *******************************************************************
 *******************************************************************/
#define BUZZER 13 
#define BRIGHT_LED 12

/******************************************************************
 *******************************************************************
 *******************************************************************/
#define X 8
#define Y 10

int MAP[X][Y];
int grid_size;

/******************************************************************
 *******************************************************************
 *******************************************************************/
#define GOAL1 222
#define GOAL2 333
#define GOAL3 444
#define GOAL4 555
#define GOAL5 666
#define GOAL5 777

int robot_x;
int robot_y;

int goal_x;
int goal_y;


int temp_x;
int temp_y;

int xd = 0;
int yd = -1;

int x = 0;
int y = 0;
.
int free = 0;
int goal = 10;
int robot = 5;
int blocked = 999;



/******************************************************************
 *******************************************************************
 *******************************************************************/
#define DRYPIN 50
#define GAMEPIN 51 
#define CHK1PIN 52
#define CHK2PIN 53 
#define CHK3PIN 54
#define CHK4PIN 55
#define RESETPIN 56

int dry_reading;
int game_reading;
int chk1_reading;
int chk2_reading;
int chk3_reading;
int chk4_reading; 
int reset_reading;

int state = 8;




/******************************************************************
 *******************************************************************
 *******************************************************************/
int grid_map[X][Y]={
  {
    0,0,0,0,0,0,0,0,0,0                                                                }
  ,{
    0,0,0,0,0,0,0,0,0,0                                                                }
  ,{
    0,0,0,0,0,0,0,0,0,555                                                                }
  ,{
    0,0,0,0,0,0,0,0,0,555                                                                }
  ,{
    0,0,0,555,0,0,0,0,0,0                                                                }
  ,{
    999,999,999,999,999,999,999,0,999,999                                                                }
  ,{
    999,999,999,999,999,999,999,0,999,999                                                                }
  ,{
    999,999,999,999,999,999,999,0,999,999                                            }
};

/******************************************************************
 *******************************************************************
 *******************************************************************/

void setup(){
  pinMode(PWM_A,OUTPUT);
  pinMode(PWM_B,OUTPUT);

  pinMode(BUZZER,OUTPUT);
  pinMode(BRIGHT_LED,OUTPUT);

  pinMode(MOTOR_RIGHT,OUTPUT);
  pinMode(MOTOR_LEFT,OUTPUT);

  for(int i = 0;i < 8;i++){
    pinMode(S[i],INPUT);
  }
  Serial.begin(9600);

}

void loop(){
  read_sensors();
  check_status();
  run();

}
/******************************************************************
 *******************************************************************
 *******************************************************************/

void read_sensors(){
  for(int i=0;i<8;i++){
    vS[i]=!(digitalRead(S[i]));
  }
  initialize(); 
}

void initialize(){
  Whiteline = (vS[0]== 1 || vS[1]== 1 || vS[2]== 1 || vS[3]== 1 || vS[4]== 1 || vS[5]== 1 || vS[6]== 1 || vS[7]== 1);
  Junction = (vS[0]== 1 && vS[1]== 1 && vS[2]== 1 && vS[3]== 1 && vS[4]== 1 && vS[5]== 1 && vS[6]== 1 && vS[7]== 1);
  Dry_end = (xd == 4 && yd == 9);
}

void buzzer(){
  digitalWrite(BUZZER,HIGH);
  delay(30);
  digitalWrite(BUZZER,LOW);
  delay(30);
}
/******************************************************************
 *******************************************************************
 *******************************************************************/
void check_status(){
  dry_reading == digitalRead(DRYPIN);
  game_reading == digitalRead(GAMEPIN);
  chk1_reading == digitalRead(CHK1PIN);
  chk2_reading == digitalRead(CHK2PIN);
  chk3_reading == digitalRead(CHK3PIN);
  chk4_reading == digitalRead(CHK4PIN); 
  reset_reading == digitalRead(RESETPIN);


  if(dry_reading == HIGH){
    state = 1;
  }

  else if(game_reading == HIGH){
    state = 2;
  }
  else if(chk1_reading == HIGH){
    state = 3;
  }
  else if(chk2_reading == HIGH){
    state = 4;
  }
  else if(chk3_reading == HIGH){
    state = 5;
  }
  else if(chk4_reading == HIGH){
    state = 6;
  }
  else if(reset_reading == HIGH){
    state = 0;
  }
  dry_reading == LOW;
  game_reading == LOW;
  chk1_reading == LOW;
  chk2_reading == LOW;
  chk3_reading == LOW;
  chk4_reading == LOW;

}
/******************************************************************
 *******************************************************************
 *******************************************************************/
void run(){
  while(state == 0){
  }
  while(state == 1){
    while(Whiteline && !Dry_end){
      forward_run();
      if(Junction && Junctionlock == 0){
        Junctionlock = 1;
        move_up();
        grid_running();
      }
      if(!Junction){
        Junctionlock = 0;
      }
    }
    soft_brake();
  }
  while(state == 2){
    form_map();
  }
  while(state == 3){
  }
  while(state == 4){
  }
  while(state == 5){
  }
  while(state == 6){
  }
}
/******************************************************************
 *******************************************************************
 *******************************************************************/
void set_coordinates(int loc){

}
void get_map_coordinates(int no){
  int i,j,k;
  for(i = 0;i < X ; i++){
    for(j = 0;j< Y;j++){
      for(k =0;k< no + 1;k++){
        if(MAP[i][j] == no){
          temp_x = i;
          temp_y = j;
        }
      }
    }
  }
}
/******************************************************************
 *******************************************************************
 *******************************************************************/
void write_to_EEPROM(){
  grid_size=sizeof(grid_map)/sizeof(grid_map[0][0]);
  for(int i=0;i<grid_size;i++){
    for(int j=0;j<X;j++){
      for(int k=0;k<Y;k++,i++){
        EEPROM.write(i,grid_map[j][k]);
      }
    }
  }
}

void form_map(){

  grid_size=sizeof(grid_map)/sizeof(grid_map[0][0]);
  for(int i = 0;i < grid_size;i++){
    for(int j = 0;j < X;j++){
      for(int k = 0;k < Y;k++,i++){
        MAP[j][k]=EEPROM.read(i);
      }
    }
  }


}




/******************************************************************
 *******************************************************************
 *******************************************************************/
void forward_run(){
  if(vS[2] ==1 && vS[3] ==1 && vS[4]==1){
    analogWrite(PWM_A,MAX_SPEED);
    analogWrite(PWM_B,SPEED1);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[3] == 1 && vS[4] ==1 && vS[5] ==1){
    analogWrite(PWM_A,SPEED1);
    analogWrite(PWM_B,MAX_SPEED);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[1] == 1 && vS[2]==1 && vS[3]==1 ){
    analogWrite(PWM_A,MAX_SPEED);
    analogWrite(PWM_B,SPEED2);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[6] == 1 && vS[5]==1 && vS[4]==1 ){
    analogWrite(PWM_A,SPEED2);
    analogWrite(PWM_B,MAX_SPEED);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[0] == 1 && vS[1]==1){
    analogWrite(PWM_A,MAX_SPEED);
    analogWrite(PWM_B,SPEED3);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[7] == 1 && vS[6]==1){
    analogWrite(PWM_A,SPEED3);
    analogWrite(PWM_B,MAX_SPEED);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[0] == 1){
    analogWrite(PWM_A,MAX_SPEED);
    analogWrite(PWM_B,BRAKE);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else if(vS[7] == 1){
    analogWrite(PWM_A,BRAKE);
    analogWrite(PWM_B,MAX_SPEED);
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
  }
  else{
    analogWrite(PWM_A,BRAKE);
    analogWrite(PWM_B,BRAKE);
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
  }
}

void turn_left(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,HIGH);
  digitalWrite(MOTOR_LEFT,LOW);
  while(!Whiteline && !(vS[2] == 1 || vS[3] == 1 && vS[4] == 1 || vS[5] == 1)){

  }
}
void turn_right(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,LOW);
  digitalWrite(MOTOR_LEFT,HIGH);
  while(!Whiteline && !(vS[2] == 1 || vS[3] == 1 && vS[4] == 1 || vS[5] == 1)){

  }
}
void low_speed(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,HIGH);
  digitalWrite(MOTOR_LEFT,HIGH);
}
void reverse(){
  analogWrite(PWM_A,MAX_SPEED);
  analogWrite(PWM_B,MAX_SPEED);
  digitalWrite(MOTOR_RIGHT,LOW);
  digitalWrite(MOTOR_LEFT,LOW);
  delay(3);
}
void soft_brake(){
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);
  delay(DELAY_SOFT_BRAKE);
}
void hard_brake(){
  if(digitalRead(MOTOR_RIGHT) == HIGH && (digitalRead(MOTOR_LEFT) == HIGH)){
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
    delay(DELAY_HARD_BRAKE);
  }
  else if(digitalRead(MOTOR_RIGHT) == LOW &&(digitalRead(MOTOR_LEFT) == LOW)){
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
    delay(DELAY_HARD_BRAKE);
  }
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);
}
void hard_brake2(){
  if(digitalRead(MOTOR_RIGHT) == HIGH && (digitalRead(MOTOR_LEFT) == HIGH)){
    digitalWrite(MOTOR_RIGHT,LOW);
    digitalWrite(MOTOR_LEFT,LOW);
    delay(DELAY_HARD_BRAKE2);
  }
  else if(digitalRead(MOTOR_RIGHT) == LOW &&(digitalRead(MOTOR_LEFT) == LOW)){
    digitalWrite(MOTOR_RIGHT,HIGH);
    digitalWrite(MOTOR_LEFT,HIGH);
    delay(DELAY_HARD_BRAKE2);
  }
  analogWrite(PWM_A,BRAKE);
  analogWrite(PWM_B,BRAKE);
}

/******************************************************************
 *******************************************************************
 *******************************************************************/
void move_up(){
  if(x % 2 == 0){
    y++;
  }
  else if(x % 2 == 1 ){
    y--;
  }
}
void grid_running(){
  if(x % 2 == 0){
    if(y == 9){
      hard_brake();
      low_speed();
      delay(250);
      hard_brake2();
      turn_right();
      hard_brake2();
      x++;
      y++;
    }
    else if(y == 0 && x != 0){
      hard_brake();
      low_speed();
      delay(250);
      hard_brake();
      turn_left();
      hard_brake();
    }
  }
  else if(x % 2 == 1){
    if(y == 9){
      hard_brake2();
      low_speed();
      delay(250);
      hard_brake2();
      turn_right();
      hard_brake2();
    }
    else if(y == 0){
      hard_brake();
      low_speed();
      delay(250);
      hard_brake2();
      turn_left();
      hard_brake2();
      x++;
      y--;
    }
  }
}
/******************************************************************
 *******************************************************************
 *******************************************************************/

int propagate_wavefront(int robot_x, int robot_y, int goal_x, int goal_y)
{
  
  unpropagate(robot_x, robot_y);

  i=0;
  while(i<50)
  {
    x=0;
    y=0;
    while(x < X && y < Y)
    {
      
      if (MAP[x][y] != wall && MAP[x][y] != goal)
      {	
        
        if (min_node(x, y) < reset_min && MAP[x][y] == robot)
        {
          
          return min_node_loc;
        }
        
        else if (min_node != reset_min)
            MAP[x][y] = min_node+1;
      }

      
      y++;
      if (y == Y && x != Y)
      {
        x++;
        y=0;
      }
    }
    i++;
  }
  return 0;
}
void unpropagate(int robot_x, int robot_y){
  for(x = 0;x < X;x++){
    for(y = 0;y < Y ; y++){
      if(MAP[x][y] != blocked && MAP[x][y] != goal){
        MAP[x][y] = free;

      }


    }

  }
  MAP[robot_x][robot_y] = robot;
  MAP[goal_x][goal_y] = goal;
}

/*If the robot was unable to find a correct path to move arage the
 map without any walls nor nodes so as to get much more paths
 this is bit error handling but wont give desired output but will
 reach the goal
 
 */

void clean_map(){
  for(x = 0;x < X;x++){
    for(y = 0; y < Y;y++){
      if(MAP[x][y] != robot && MAP[x][y] != goal){
        MAP[x][y] = free;
      }

    }

  }

}

/* Finding the minimum surrounding node coordinate */
int min_node(int x,int y){
  min_node = reset_min;

  //Go North
  if(x > ){
    if(MAP[x-1][y] < min_node && MAP[x-1][y] != nothing){
      min_node = MAP[x-1][y];
      min_node_loc = 1;


    }
    //go East
  }
  if(y < Y){

    if(MAP[x][y+1] < min_node && MAP[x][y+1] != nothing){
      min_node = MAP[x][y+1];
      min_node_loc = 2;

    }//go South

  }
  if(x < X){
    if(MAP[x+1][y] < min_node && MAP[x+1][y] != nothing){
      min_node = MAP[x+1][y];
      min_node_loc = 3;


    }
    //go West
  }
  if(y > 0){
    if(MAP[x][y-1] < min_node && MAP[x][y-1] != nothing){
      min_node = MAP[x][y-1];
      min_node_loc = 4;

    }


  }
  return min_node;
}





